{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

-- vim:expandtab

module Main (main) where

import Control.Applicative ((<|>))
import Control.Monad (when, forever)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Resource
import Data.ByteString.UTF8 as B8
import qualified Data.ByteString as BS
import Data.Functor ((<&>))
import Data.List.Extra (splitOn)
import Data.Maybe (fromMaybe)
import System.Environment (getArgs)
import System.IO

import Network.WebSockets (Connection)

import KeksCrypto
import Opts
import Signaling
import GenPass
import qualified Webrtc as W

tryOpenFile :: FilePath -> IOMode -> IO Handle
tryOpenFile p m = openFile p m <|> exitMsg ("Couldn't open file: " ++ p)

sendSig :: Connection
    -> ClientId -- our client id
    -> ClientId -- remote client id
    -> KeksEncrypt
    -> W.SignalSender
sendSig c myId cId enc getSig = forever $ do
    msg <- getSig <&> \case
        W.Offer sdp      -> OfferMessage sdp
        W.Answer sdp     -> AnswerMessage sdp
        W.Candidate cand -> IceCandidateMessage cand

    sendRelay c enc myId cId msg

receiveSig :: Connection
    -> ClientId -- remote client id
    -> KeksDecrypt
    -> W.SignalReceiver
receiveSig c cId dec cb = forever $ do
    receiveMessage c >>= \case
        Left bs -> exitMsg $ "Didn't understand packet: " ++ show bs
            ++ "\nThis is a bug, and should probably be reported."

        -- TODO these should check whether the sender is our peer
        Right (PkMsg _ encMsg) -> case parseRelay encMsg dec of
            Left err -> exitMsg $ "Couldn't decrypt " ++ encMsg ++ ": " ++ err
            Right (_, OfferMessage sdp)         -> cb (W.Offer sdp)
            Right (_, AnswerMessage sdp)        -> cb (W.Answer sdp)
            Right (_, IceCandidateMessage cand) -> cb (W.Candidate cand)
            _ -> return () -- ignore any other relayed messages

        -- ignore any other server messages, and just repeat
        _ -> return ()

sendToDc name dc h = do
    let assert m = m >>= \b -> when (not b) $ exitMsg "Connection closed early"

    assert $ W.sendMessage dc $ B8.fromString name
    putStrLn "sent file name"

    let go = do
            some <- BS.hGet h (2^14) -- 16K
            if BS.null some
            then return ()
            else assert (W.sendMessage dc some) >> go
    go
    putStrLn "Done sending file."

send args = runResourceT $ do
    -- parse options
    flags <- liftIO $ parseSendOpts args
    pass <- liftIO $ if sPass flags /= ""
                     then return $! sPass flags
                     else getPass
    let (dec, enc, room) = getCrypto $ B8.fromString pass

    -- open file
    h <- if sFile flags == "-"
         then return $! stdin
         else snd <$> allocate (tryOpenFile (sFile flags) ReadMode) hClose
    liftIO $ putStrLn $ "On the other side, use: rift receive -k " ++ pass

    -- main loop
    let go myId c = receiveMessage c >>= \case
            -- decoding packet failed; this packet came from the server, so consider
            -- this a bug on our half.
            Left bs -> exitMsg $ "Didn't understand packet: " ++ show bs
                ++ "\nThis is a bug, and should probably be reported."

            -- we don't care a lot about these two.
            Right (PkInit newId _) -> putStrLn "Waiting for receiver..." >> go newId c
            Right (PkLeave _) -> go myId c

            Right (PkJoin cId) ->
                -- don't send anything if this is about ourselves.
                if cId == myId
                then go myId c
                else do
                    -- someone else is here! tell them we offer a file.
                    -- sendIdentify myId c cId -- TODO
                    sendRelay c enc myId cId $ ChatMessage "OFFER"
                    go myId c

            Right (PkMsg _ encMsg) -> case parseRelay encMsg dec of
                Left err -> exitMsg $ "Couldn't decrypt " ++ encMsg ++ ": " ++ err
                Right (rId, OfferMessage sdp) -> W.answerChannel
                        (sIces flags)
                        (sendSig c myId rId enc)
                        (\cb -> cb (W.Offer sdp) >> receiveSig c rId dec cb)
                            $ \dc _ -> sendToDc (sFile flags) dc h
                Right _ -> go myId c

    -- enter main loop
    liftIO $ runSignaling (sSignalServer flags) room $ go (-1)

receiveFromDc dc flags = do
    putStrLn $ "Receiving file name..."
    unsafeRemoteName <- W.receiveMessage dc >>= \case
        Nothing -> exitMsg "Connection closed early."
        Just s -> return $! B8.toString s
    
    putStrLn $ "Receiving" ++ unsafeRemoteName
    when ('/' `elem` unsafeRemoteName)
        $ putStrLn "Remote name contains '/'; using base name"

    let remoteName = last $ splitOn "/" unsafeRemoteName
        name = fromMaybe remoteName $ rFile flags

    when (name == "") $ exitMsg "Empty file name; exiting."

    withFile name WriteMode $ \h ->
        let go = do
                W.receiveMessage dc >>= \case
                    Nothing -> return ()
                    Just bs -> BS.hPut h bs >> go
        in go

receive args = do
    flags <- parseReceiveOpts args
    let (dec, enc, room) = getCrypto $ B8.fromString $ rPass flags

    let go myId c = receiveMessage c >>= \case
            -- decoding packet failed; this packet came from the server, so consider
            -- this a bug on our half.
            Left bs -> exitMsg $ "Didn't understand packet: " ++ show bs
                ++ "\nThis is a bug, and should probably be reported."

            Right (PkInit newId _) -> go newId c
            Right (PkLeave _) -> go myId c
            Right (PkJoin _) -> go myId c -- TODO send identification

            Right (PkMsg _ encMsg) -> case parseRelay encMsg dec of
                Left err -> exitMsg $ "Couldn't decrypt " ++ encMsg ++ ": " ++ err
                Right (sId, ChatMessage "OFFER") -> W.offerChannel
                    "blubbel"
                    (rIces flags)
                    (sendSig c myId sId enc)
                    (receiveSig c sId dec)
                        $ \dc _ -> receiveFromDc dc flags
                Right _ -> go myId c

    liftIO $ runSignaling (rSignalServer flags) room $ go (-1)

main = getArgs >>= \case
        "send"    : opts -> send opts
        "receive" : opts -> receive opts
        _ -> exitMsg "arguments must begin with \"send\" or \"receive\""
