# rift
NOTE: due to limitations of the haskell FFI (namely it not supporting re-entering haskell-land asynchronously), this project was abandoned.  
If this is ever going to be supported, this code should work with very minor changes.


Bad/WIP alternative to the magic wormhole.

The goal of this project is to be a peer-to-peer solution for easy file transfer over the internet, from the CLI. A signaling server will still be used, but it will get very little information about your activity.

The protocol builds on MetaMuffins [keks-meet](https://codeberg.org/metamuffin/keks-meet/)-protocol, similar to their attempt at creating a tool like this (though they will probably finish this much quicker than I'll manage, if I finish this at all. lol)
