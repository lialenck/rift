module KeksCrypto (getCrypto, KeksDecrypt, KeksEncrypt, KeksRoom) where

import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types (cipherInit, aeadInit, aeadSimpleDecrypt, aeadSimpleEncrypt, AuthTag(..), AEADMode(AEAD_GCM))
import Crypto.Error (CryptoFailable(..))
import Crypto.KDF.PBKDF2 (fastPBKDF2_SHA256, Parameters(..))
import Crypto.Hash (Digest, hash)
import Crypto.Hash.Algorithms (SHA256)
import Data.ByteArray.Encoding (convertFromBase, convertToBase, Base(Base64))
import Data.ByteArray (convert)
import Data.Functor ((<&>))
import System.Random (getStdGen, randoms)

import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as B8
import Data.Either.Extra (maybeToEither)

salt :: BS.ByteString
salt = case convertFromBase Base64 (B8.fromString "thisisagoodsaltAAAAAAA==") of
           Right e -> e
           Left _ -> error "impossible!"

digestPw :: BS.ByteString -> BS.ByteString
digestPw pw = fastPBKDF2_SHA256 (Parameters 250000 32) pw salt

cfToEither :: CryptoFailable a -> Either String a
cfToEither (CryptoPassed a) = Right a
cfToEither (CryptoFailed e) = Left $! show e

decryptMessage :: BS.ByteString -> String -> Either String BS.ByteString
decryptMessage key msg = do
    msgBs <- convertFromBase Base64 (B8.fromString msg)
    let (iv, more)        = BS.splitAt 12 msgBs
        (encMsg, authTag) = BS.splitAt (BS.length more - 16) more
    
    aes  <- cfToEither $ cipherInit key :: Either String AES256
    aead <- cfToEither $ aeadInit AEAD_GCM aes iv

    maybeToEither "Couldn't decrypt"
        $! aeadSimpleDecrypt aead BS.empty encMsg (AuthTag $ convert authTag)

encryptMessage :: BS.ByteString -> BS.ByteString -> IO (Either String String)
encryptMessage key msg = getStdGen <&> \gen -> do
    let iv = BS.pack $ take 12 $ randoms gen

    aes  <- cfToEither $ cipherInit key :: Either String AES256
    aead <- cfToEither $ aeadInit AEAD_GCM aes iv

    let (AuthTag authTag, encMsg) = aeadSimpleEncrypt aead BS.empty msg 16
        msgRaw = iv <> encMsg <> convert authTag

    return $! B8.toString $! convertToBase Base64 msgRaw

hashKey :: BS.ByteString -> String
hashKey k = show (hash $ B8.fromString "also-a-very-good-salt" <> k :: Digest SHA256)

-- encrypting will use IO later for IV generation
type KeksDecrypt =        String ->     Either String BS.ByteString
type KeksEncrypt = BS.ByteString -> IO (Either String        String)
type KeksRoom = String

getCrypto :: BS.ByteString -> (KeksDecrypt, KeksEncrypt, KeksRoom)
getCrypto pw = let k = digestPw pw
    in (decryptMessage k, encryptMessage k, hashKey pw)
