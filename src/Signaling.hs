{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Signaling (
    ClientId,
    ClientPk(..),
    ServerPk(..),
    RelayMessage(..),

    receiveMessage,
    runSignaling,
    sendRelay,
    parseRelay
) where

import Control.Exception (catch)
import Data.Aeson
import Data.Aeson.KeyMap (toList, fromList)
import Data.Aeson.Types (parse)
import Data.ByteString.Lazy (ByteString, toStrict, fromStrict)
import Data.Either.Extra (maybeToEither)
import Data.Functor ((<&>))
import Data.List.Extra
import Data.Maybe (fromMaybe)
import Network.WebSockets
import Text.Read (readMaybe)
import Wuss (runSecureClient)

import KeksCrypto
import Opts

type ClientId = Int

data ClientPk =
    PkInit ClientId String | PkJoin ClientId | PkLeave ClientId | PkMsg ClientId String
    deriving (Show, Eq)

instance FromJSON ClientPk where
    parseJSON = withObject "ClientPk" $ \m -> case toList m of
        [("init", Object ob)]         -> PkInit  <$> ob .: "your_id" <*> ob .: "version"
        [("client_join", Object ob)]  -> PkJoin  <$> ob .: "id"
        [("client_leave", Object ob)] -> PkLeave <$> ob .: "id"
        [("message", Object ob)]      -> PkMsg   <$> ob .: "sender" <*> ob .: "message"
        _ -> mempty

data ServerPk = PkPing | PkRelay ClientId String deriving (Show, Eq)

instance ToJSON ServerPk where
    -- TODO: for a tiny bit of performance, but mostly practice, implement toEncoding
    toJSON PkPing = Object $ fromList [("ping", Null)]
    toJSON (PkRelay rep msg) = object ["relay" .= object [
        "recipient" .= rep, "message" .= msg]]

-- only the inner part; a decoder should also decode the sender and return as a tuple.
-- I could've made a wrapper type that contains the sender, but that felt overcomplicated.
-- also; the second string of OfferMessage could be an enum type
data RelayMessage = ChatMessage String | IdentifyMessage String | OfferMessage String | AnswerMessage String | IceCandidateMessage String deriving (Show, Eq)

instance FromJSON RelayMessage where
    parseJSON = withObject "RelayMessage" $ \m -> case toList m of
        [("chat", Object ob)] -> ChatMessage <$> ob .: "text"
        [("identify", Object ob)] -> IdentifyMessage <$> ob .: "username"
        [("offer", Object ob)] -> OfferMessage <$> ob .: "sdp"
        [("answer", Object ob)] -> AnswerMessage <$> ob .: "sdp"
        [("ice_candidate", Object ob)] -> IceCandidateMessage <$> ob .: "candidate"
        _ -> mempty

instance ToJSON RelayMessage where
    -- TODO: for a tiny bit of performance, but mostly practice, implement toEncoding
    toJSON (ChatMessage msg) = object ["chat" .= object ["text" .= msg]]
    toJSON (IdentifyMessage i) = object ["identify" .= object ["username" .= i]]
    toJSON (OfferMessage sdp) = object ["offer" .= object [
        "sdp" .= sdp, "type" .= ("offer" :: String)]]
    toJSON (AnswerMessage sdp) = object ["answer" .= object [
        "sdp" .= sdp, "type" .= ("answer" :: String)]]
    toJSON (IceCandidateMessage cand) = object ["ice_candidate" .= object [
        "candidate" .= cand]]

receiveMessage :: Connection -> IO (Either ByteString ClientPk)
receiveMessage c = do
    bs <- receiveDataMessage c <&> \case
        Text bs _ -> bs
        Binary bs -> bs

    return $! maybeToEither bs $ decode' bs

runSignaling ss room f =
    let (origin, pathBegin) = breakOn "/" $ last $ splitOn "://" ss -- also remove protocol
        (host, mayPort) = readMaybe <$> breakOn ":" origin :: (String, Maybe Int)
        port = fromIntegral $ fromMaybe 443 mayPort
        path = pathBegin ++ room
    in                                        runSecureClient host port path f
        `catch` \(_ :: HandshakeException) -> runClient       host port path f

sendRelay :: Connection -> KeksEncrypt -> ClientId -> ClientId -> RelayMessage -> IO ()
sendRelay c enc myId cId msg = do
    mayEncMsg <- enc $ toStrict $ encode $ object ["sender" .= myId,
            "inner" .= msg]
    encMsg <- case mayEncMsg of
        Left err -> exitMsg $ "Failed to encrypt: " ++ err
        Right m -> return m

    sendTextData c $ encode $ PkRelay cId encMsg

parseRelay :: String -> KeksDecrypt -> Either String (ClientId, RelayMessage)
parseRelay encMsg dec = do
    msgBs <- case dec encMsg >>= eitherDecode' . fromStrict of
        Left err -> error $ "Failed to decrypt: " ++ err
        Right m -> Right m

    let parseOuter = withObject "Relay Wrapper"
            $ \o -> (,) <$> o .: "sender" <*> o .: "inner"

    case parse parseOuter msgBs of
        Error s -> Left s
        Success a -> Right a
