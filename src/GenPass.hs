module GenPass (getPass) where

import System.Random

getPass :: IO String
getPass = show <$> (randomIO :: IO Int)
