{-# LANGUAGE TemplateHaskell #-}

module Opts (
    exitMsg,

    ReceiveOpts(..),
    parseReceiveOpts,
    SendOpts(..),
    parseSendOpts
)
where

import Data.Default
import Data.List (foldl')
import Data.List.Split (splitOn)
import System.Console.GetOpt
import System.Exit (exitFailure, exitSuccess)
import System.IO (hPutStr, stderr)

import GenPass

-- utility function: optionally output an error message (if msg is not empty), and exit
exitMsg msg = 
    if msg == ""
    then exitSuccess
    else hPutStr stderr (msg ++ "\n") >> exitFailure

data ReceiveOpts = ReceiveOpts {
    rIces :: [String],
    rFile :: Maybe String,
    rSignalServer :: String,
    rPass :: String
} deriving Show

data SendOpts = SendOpts {
    sIces :: [String],
    sFile :: String,
    sSignalServer :: String,
    sPass :: String
} deriving Show

instance Default ReceiveOpts where
    def = ReceiveOpts ["meet.metamuffin.org:16900", "stun.ekiga.net"]
        Nothing "meet.metamuffin.org/signaling/" ""

instance Default SendOpts where
    def = SendOpts ["meet.metamuffin.org:16900", "stun.ekiga.net"]
        "-" "meet.metamuffin.org/signaling/" ""


data ReceiveFlag = RHelp | RFile String | RIceServers [String] | RSignalServer String | RKey String
    deriving (Show, Eq) 

receiveOpts :: [OptDescr ReceiveFlag]
receiveOpts = [
    Option "h" ["help"] (NoArg RHelp)
        "show help",
    Option "o" ["output"] (ReqArg RFile "FILE")
        "force output file name (leave out to let the sender decide)",
    Option ""  ["ice-servers"] (ReqArg (RIceServers . splitOn ",") "URLS")
        "comma-seperated list of stun/turn servers",
    Option ""  ["signal-server"] (ReqArg RSignalServer "URL")
        "signaling server",
    Option "k" ["key"] (ReqArg RKey "PASSPHRASE")
        "passphrase (for non-interactive use)" ]

data SendFlag = SHelp | SIceServers [String] | SSignalServer String | SKey String
    deriving (Show, Eq) 

sendOpts :: [OptDescr SendFlag]
sendOpts = [
    Option "h" ["help"] (NoArg SHelp)
        "show help",
    Option ""  ["ice-servers"] (ReqArg (SIceServers . splitOn ",") "URLS")
        "comma-seperated list of stun/turn servers",
    Option ""  ["signal-server"] (ReqArg SSignalServer "URL")
        "signaling server",
    Option "k" ["key"] (ReqArg SKey "PASSPHRASE")
        "passphrase (leave out to automatically generate a secure one)" ]

receiveUsage e = do
    putStr $ usageInfo "Usage: receive [OPTIONS], where OPTIONS are:" receiveOpts
    exitMsg e

parseReceiveOpts args = do
    rOpts <- case getOpt Permute receiveOpts args of
            -- print usage on getopt errors
            (_, _, e : _) -> receiveUsage e
            (_, a : _, _) -> receiveUsage $ "Extra argument: " ++ a
            (flags, _, _) ->
                if RHelp `elem` flags
                then receiveUsage ""
                else return (foldl' applyOpt def flags)

    if rPass rOpts /= ""
    then return $! rOpts
    else do
        hPutStr stderr "Please gib passphrase: "
        p <- getLine
        return $! rOpts { rPass = p }

  where applyOpt o (RFile x)         = o { rFile = Just x }
        applyOpt o (RIceServers x)   = o { rIces = x }
        applyOpt o (RSignalServer x) = o { rSignalServer = x }
        applyOpt o (RKey x)          = o { rPass = x }
        applyOpt _ _ = error "impossible!"

parseSendOpts args = do
    sOpts <- case getOpt Permute sendOpts args of
            -- print usage on getopt errors
            (_, _, e : _)       -> receiveUsage e
            (_, _ : a : _, _)   -> receiveUsage $ "Extra argument: " ++ a
            (flags, mayFile, _) ->
                if SHelp `elem` flags
                then receiveUsage ""
                else return $ flip (foldl' applyOpt) flags $ case mayFile of
                    []    -> def
                    f : _ -> def { sFile = f }

    if sPass sOpts /= ""
    then return $! sOpts
    else do
        randKey <- getPass
        return $! sOpts { sPass = randKey }

  where applyOpt o (SIceServers x)   = o { sIces = x }
        applyOpt o (SSignalServer x) = o { sSignalServer = x }
        applyOpt o (SKey x)          = o { sPass = x }
        applyOpt _ _ = error "impossible!"
