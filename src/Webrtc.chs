-- vim:expandtab

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-} -- TODO only for debugging

module Webrtc (
    offerChannel,
    answerChannel,

    Signal(..),
    SignalSender,
    SignalReceiver,
    DatachannelApp,
    RtcConfig,
    Datachannel,

    sendMessage,
    receiveMessage
) where

import Control.Concurrent (threadDelay) -- TODO bad
import Control.Concurrent.Async (withAsync)
import Control.Monad (forM_, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Resource
import Foreign.C.String (CString, newCString, peekCString, withCString)
import Foreign.C.Types
import Foreign.ForeignPtr (withForeignPtr)
import Foreign.Marshal.Alloc (mallocBytes, callocBytes, free)
import Foreign.Ptr
import Foreign.Storable
import UnliftIO.Chan
import UnliftIO.Exception (bracket)
import UnliftIO.MVar
import UnliftIO.IORef
import qualified Data.ByteString.Internal as BSI

#include <rtc/rtc.h>

data Datachannel = Datachannel CInt (Chan (Maybe BSI.ByteString))
type RtcConfig = [String]

data Signal = Offer String | Answer String | Candidate String deriving (Show, Eq)

-- will be called once, and should forward signals from the callback to the peer.
type SignalSender   = IO Signal -> IO ()
-- will be called once, and should forward signals using the callback.
type SignalReceiver = (Signal -> IO ()) -> IO ()
-- 1st. arg: library-internal type representing a channel
-- 2nd. arg: channel name
-- TODO make this parametric on its return type
type DatachannelApp a = Datachannel -> String -> IO a

ptrSize = sizeOf (undefined :: Ptr ())

createPc :: RtcConfig -> IO CInt
createPc ices = do
    let n = length ices
    rtcCfg <- callocBytes {#sizeof rtcConfiguration#}
    iceArr <- mallocBytes $ ptrSize * n

    forM_ [0..n-1] $ \i -> do
        poke (plusPtr iceArr $ i * ptrSize) =<< newCString (ices !! i)

    {#set rtcConfiguration.iceServers#}      rtcCfg iceArr
    {#set rtcConfiguration.iceServersCount#} rtcCfg $ fromIntegral n
    r <- {#call rtcCreatePeerConnection #} rtcCfg

    forM_ [0..n-1] $ \i -> do
        free =<< peek (plusPtr iceArr $ i * ptrSize)

    free iceArr
    free rtcCfg
    when (r < 0) $ error "Couldn't init libdatachannel"
    return $! r

foreign import ccall safe "wrapper" makeLocalDescCb
    ::            (CInt -> CString -> CString -> Ptr () -> IO ())
    -> IO (FunPtr (CInt -> CString -> CString -> Ptr () -> IO ()))

foreign import ccall safe "wrapper" makeLocalCandCb
    ::            (CInt -> CString -> CString -> Ptr () -> IO ())
    -> IO (FunPtr (CInt -> CString -> CString -> Ptr () -> IO ()))

foreign import ccall safe "wrapper" makeDcCb
    ::            (CInt -> CInt -> Ptr () -> IO ())
    -> IO (FunPtr (CInt -> CInt -> Ptr () -> IO ()))

{#enum rtcState as RtcState {underscoreToCase} deriving (Show, Eq)#}
foreign import ccall safe "wrapper" makeStateCb
    ::            (CInt -> CInt -> Ptr () -> IO ())
    -> IO (FunPtr (CInt -> CInt -> Ptr () -> IO ()))

foreign import ccall safe "wrapper" makeOpenCb
    ::            (CInt -> Ptr () -> IO ())
    -> IO (FunPtr (CInt -> Ptr () -> IO ()))
makeCloseCb = makeOpenCb

foreign import ccall safe "wrapper" makeMessageCb
    ::            (CInt -> Ptr CChar -> CInt -> Ptr () -> IO ())
    -> IO (FunPtr (CInt -> Ptr CChar -> CInt -> Ptr () -> IO ()))

assertGt0 m = do
    r <- m
    when (r < 0) $ error "oh no"

runDatachannelApp ::
       Maybe String
    -> (String -> Signal)
    -> RtcConfig
    -> SignalSender
    -> SignalReceiver
    -> DatachannelApp a
    -> IO (Either String a)
runDatachannelApp name lDescType ices ss sr f = runResourceT $ do
    liftIO $ {#call rtcInitLogger#} 4 nullFunPtr -- TODO bad

    sendChan <- newChan
    recvMsgChan <- newChan

    dcOpened <- newIORef False
    retVal <- newEmptyMVar

    (pcKey, pc) <- allocate (createPc ices) $ \pc ->
        {#call rtcDeletePeerConnection#} pc >> return ()

    let sendLDesc _ sdp  _ _ = writeChan sendChan . lDescType =<< peekCString sdp
        sendCand  _ cand _ _ = writeChan sendChan . Candidate =<< peekCString cand
        stCallback _ st _ = case toEnum $ fromIntegral st of
            RtcClosed -> do
                putStrLn "\nDetected connection close.\n"
                writeChan recvMsgChan Nothing
                wasOpen <- readIORef dcOpened
                when (not wasOpen) $ putMVar retVal
                    $ Left "Closed before being properly open."
            _ -> return ()

        messageCb _ p szC _ = do
            putStrLn "GOT MESSAGE"
            let sz = fromIntegral $ szC
            when (sz < 0) $ error "fuck this library. just why???"
            bs <- BSI.create sz (\bsp -> BSI.memcpy bsp (castPtr p) sz)
            writeChan recvMsgChan $ Just bs

        dcOnClose _  _ = writeChan recvMsgChan Nothing
        dcOnOpen  dc _ = do
            writeIORef dcOpened True

            sendMessage (Datachannel dc recvMsgChan) "wubbelel" -- TODO bad

            sz <- {#call rtcGetDataChannelLabel#} dc nullPtr 0
            assertGt0 $ return sz
            label <- bracket (mallocBytes $ fromIntegral sz) free $ \buf ->
                {#call rtcGetDataChannelLabel#} dc buf sz >> peekCString buf
            r <- f (Datachannel dc recvMsgChan) label

            {#call rtcClose#} dc
            {#call rtcDeleteDataChannel#} dc
            -- in order to return a value, we want an RtcClosed message to be sent
            -- to stCallback. for that, we close the connection.
            release pcKey
            putStrLn "Closed & deleted peer connection"
            putMVar retVal $ Right r

    (_, sendLDescC)   <- allocate (makeLocalDescCb sendLDesc)  freeHaskellFunPtr
    (_, sendCandC)    <- allocate (makeLocalCandCb sendCand)   freeHaskellFunPtr
    (_, stCallbackC)  <- allocate (makeStateCb     stCallback) freeHaskellFunPtr
    (_, dcOnOpenC)    <- allocate (makeOpenCb      dcOnOpen)   freeHaskellFunPtr
    (_, dcOnCloseC)   <- allocate (makeCloseCb     dcOnClose)  freeHaskellFunPtr
    (_, messageC)     <- allocate (makeMessageCb   messageCb)  freeHaskellFunPtr

    -- this will only be called on the answering side.
    -- thus, the same kind of setup is done directly after rtcCreateDataChannel
    -- for the offering side.
    let dcCallback _ dc _ = do
            assertGt0 $ {#call rtcSetOpenCallback#}    dc dcOnOpenC
            assertGt0 $ {#call rtcSetClosedCallback#}  dc dcOnCloseC
            assertGt0 $ {#call rtcSetMessageCallback#} dc messageC
            return ()
    (_, dcCallbackC) <- allocate (makeDcCb dcCallback) freeHaskellFunPtr

    (_, offerC)  <- allocate (newCString "offer")  free
    (_, answerC) <- allocate (newCString "answer") free

    let fwdSignal sig = do
            case sig of
                Offer sdp  -> withCString sdp $ \csdp ->
                    {#call rtcSetRemoteDescription#} pc csdp offerC
                Answer sdp -> withCString sdp $ \csdp ->
                    {#call rtcSetRemoteDescription#} pc csdp answerC
                Candidate cand -> withCString cand $ \ccand ->
                    {#call rtcAddRemoteCandidate#} pc ccand nullPtr
            return ()

    r <- liftIO $ do
        -- I'm assuming these can't error.
        assertGt0 $ {#call rtcSetLocalDescriptionCallback#} pc sendLDescC
        assertGt0 $ {#call rtcSetLocalCandidateCallback#}   pc sendCandC
        assertGt0 $ {#call rtcSetStateChangeCallback#}      pc stCallbackC
        assertGt0 $ {#call rtcSetDataChannelCallback#}      pc dcCallbackC

        withAsync (ss (readChan sendChan)) $ \_ ->
            withAsync (sr fwdSignal) $ \_ -> do
                case name of
                    Nothing -> return undefined
                    Just s -> withCString s $ \clabel -> do
                        dc <- {#call rtcCreateDataChannel#} pc clabel
                        {#call rtcSetOpenCallback#}    dc dcOnOpenC
                        {#call rtcSetClosedCallback#}  dc dcOnCloseC
                        {#call rtcSetMessageCallback#} dc messageC
                takeMVar retVal

    -- release pc manually (just in case that didnt already happen, which it
    -- probably did), as we definitely should release it before releasing
    -- the wrapped callbacks
    release pcKey

    return $! r

offerChannel ::
       String         -- Channel name
    -> RtcConfig
    -> SignalSender
    -> SignalReceiver
    -> DatachannelApp a
    -> IO (Either String a)
offerChannel name = runDatachannelApp (Just name) Offer

answerChannel ::
       RtcConfig
    -> SignalSender
    -> SignalReceiver
    -> DatachannelApp a
    -> IO (Either String a)
answerChannel = runDatachannelApp Nothing Answer

sendMessage :: Datachannel -> BSI.ByteString -> IO Bool
sendMessage (Datachannel dcId _) bs = do
    let (fp, off, sz) = BSI.toForeignPtr bs
    isOpen <- {#call rtcIsOpen#} dcId
    if isOpen == 0
    then return False
    else withForeignPtr fp $ \p -> do
        r <- {#call rtcSendMessage#} dcId (castPtr $ plusPtr p off) $ fromIntegral sz
        return $! r >= 0

receiveMessage :: Datachannel -> IO (Maybe BSI.ByteString)
receiveMessage (Datachannel _ chan) = readChan chan
